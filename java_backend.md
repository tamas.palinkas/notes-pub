# Project dependencies
* devtools
* web
* thymeleaf
* jpa
* mysql

# JSON and REST controller

#### annotations

```java
@JsonProperty
@JsonIgnore
```

#### Object

```java
public class Course {

    private String id;
    private String name;
    private String description;

    public Course() {
    }

    public Course(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
    //getters, setters
}
```

#### Controller

**RestController OR Controller & ResponseBody**

```java
@GetMapping(path = "/courses/{id}")
    Course getCourseById(@PathVariable String id) {
        return coursesService.getCourseById(id);
    }
```

# Error handling

## Throwing custom error and handling it in the Controller

#### Service

```java
public Tweet findById(Long id) throws TweetNotFoundException {
    Optional<Tweet> tweet = repository.findById(id);
    if (tweet.isPresent()) {
      return tweet.get();
    }
    throw new TweetNotFoundException();
  }
```

#### Controller

```java
@GetMapping("/tweets/{id}")
  ResponseEntity<Tweet> findTweetById(@PathVariable Long id) throws TweetNotFoundException {
    Tweet tweet = service.findById(id);
    return ResponseEntity.ok(tweet);
  }

  @ExceptionHandler(TweetNotFoundException.class)
  ResponseEntity tweetNotFound() {
    return ResponseEntity.notFound().build();
  }
```

#### Exception

```java
public class TweetNotFoundException extends Exception {
}
```


### Custom error with controllers

#### Controller

```java
@GetMapping("/doubling")
  public Doubled doubling(@RequestParam(value = "input", required = false) Integer input) {
    if (input != null) {
      return new Doubled(input);
    } else {
      throw  new UnsupportedOperationException("Please provide an input!");
    }
  }
```

#### ControllerAdvice (status code)

```java
@ControllerAdvice
public class NotFoundAdvice {

  @ResponseBody
  @ExceptionHandler(UnsupportedOperationException.class)
  @ResponseStatus(HttpStatus.OK)
  ErrorMessage notFoudHandler(UnsupportedOperationException ex){
    return new ErrorMessage(ex.getMessage());
  }
}
```


# Redirecting with FlashAttribute

```java
@PostMapping("/register")
  public String saveUser(@ModelAttribute User user,
                         RedirectAttributes attributes) {
    if (userService.isValidUser(user)) {
      userService.saveUser(user);
      return "redirect:/";
    }
    attributes.addFlashAttribute("error", "The username field is empty");
    return "redirect:/register";
  }
```

```java
@GetMapping("/register")
  public String showRegister(@ModelAttribute String error,
                             Model model) {
    if (userService.findUserById(1L).isPresent()) {
      return "redirect:/";
    } else {
      if (!error.trim().isEmpty()) {
        model.addAttribute("error", error);
      }
      return "register";
    }
  }
```

# API responses

```java
@GetMapping("/api/links")
  @ResponseBody
  public List<URL> getUrls() {
    return urlService.getAllUrls();
  }

  @DeleteMapping("/api/links/{id}")
  @ResponseBody
  public ResponseEntity deleteBySecretCode(@PathVariable Long id,
                                           @RequestBody SecretCode secretCode) {

    if (urlService.findById(id) == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    } 
  }
```

# Spring JPA, Hibernate

### Create auto-incremented ID for entity

```java
@Entity
public class Todo {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private String title;
  private boolean isUrgent;
  private boolean isDone;

  
  public Todo(String title) {
    this();
    this.title = title;
  }

  public Todo() {
    this.isUrgent = false;
    this.isDone = false;
  }
```
### Interface repository, which takes the ID and entity
```java
public interface TodoRepository extends CrudRepository<Todo, Long> {
}
```

### For JPA to get booleans rename getters
```java
public boolean getUrgent() {
    return urgent;
  }

public boolean getDone() {
    return done;
  }
```

### JPA search by object fields

```java
public interface TodoRepository extends CrudRepository<Todo, Long> {
  List<Todo> findAllByDone(Boolean isActive);
}
```

### Custom query for Class fields

```java
 @Query("select t from Todo t where t.title like %?1% or t.description like %?1%")
  List<Todo> findAllByContentOrDescription(String s);
```

### One to many connection

#### Todo class(many)

```java
@Entity
public class Todo {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private String title;
  private boolean urgent;
  private boolean done;
  @Temporal(TemporalType.DATE)
  private Date dateAdded;
  private String dueDate;

  @ManyToOne
  private Assignee assignee;

  public Todo(String title) {
    this();
    this.title = title;
  }

  public Todo() {
    this.urgent = false;
    this.done = false;
    this.dateAdded = new Date();
  }
  
  //getters, setters..
```

#### Assignee class(one)

```java
@Entity
public class Assignee {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private String email;
  private String name;

  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST, mappedBy = "assignee")
  private List<Todo> todos;

  public Assignee(String name, String email) {
    this.name = name;
    this.email = email;
  }

  public Assignee() {
    todos = new ArrayList<>();
  }
  
  //getters, setters..
```

### Spring JPA properties with SQL (application.properties)
```
spring.datasource.url=jdbc:mysql://localhost/database
spring.datasource.username=${SQL_UNAME}
spring.datasource.password=${SQL_PWD}
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
spring.logging.level.org.hibernate.SQL=debug
spring.jpa.show-sql=true
```

# Views, models

### Map an object from ModelAttribute

#### Class
```java
public class Todo {
  private String title;
  
  //getters, setters, other fields..
}
```

#### HTML
```html
<form th:action="@{/todo/add}" method="post">
    <input type="text" name="title">
    <input type="submit">
</form>
```
#### Controller

```java
@PostMapping("/add")
  public String addTodo(@ModelAttribute Todo todo) {
    todoService.saveTodo(todo);
    return "redirect:/todo/";
  }
```

# HTML and Thymeleaf

**name="drink" AND th:field="${fox}" Does NOT work TOGETHER**

### Thymeleaf IF
```html
<div th:if="*{tricks.isEmpty()}" th:text="'You know no tricks, yet. Go and learn some'"></div>
<div th:unless="*{tricks.isEmpty()}">
    <ul>
        <li th:each="trick : *{tricks}" th:text="${trick}"/>
    </ul>
</div>
```

### Thymeleaf object handling
```html
<body th:object="${fox}">
<div><p th:text="|This is *{name} Fox Currently living on *{food} and *{drink} He knows *{tricks.size()} tricks.|"></p></div>
```

### Th unordered list
```html
<div th:object="${todos}">
    <ul th:each="todo : ${todos}">
        <li th:text="${todo.title}"></li>
    </ul>
</div>
```

### Th table
```html
<table>
    <tr data-th-each="user : ${users}">
        <td data-th-text="${user.login}">...</td>
        <td data-th-text="${user.name}">...</td>
    </tr>
</table>
```
```html
<table>
    <tr>
        <th>ID</th>
        <th>ACTION</th>
        <th>DONE</th>
        <th>URGENT</th>
    </tr>
    <th:block th:each="todo : ${todoList}">
        <tr>
            <td th:text="${todo.getId()}"></td>
            <td th:text="${todo.getTitle()}"></td>
            <td th:text="${todo.getDone()}"></td>
            <td th:text="${todo.getUrgent()}"></td>
            <td><a th:href="@{/todo/delete(id=${todo.getId()})}">delete</a></td>
            <td><a th:href="@{/todo/update(id=${todo.getId()})}">update</a></td>
        </tr>
    </th:block>
</table>
```

### Simple login form

```html
<form th:action="@{/login}" method="post">
    <input type="text" name="userName">
    <input type="submit">
</form>
```