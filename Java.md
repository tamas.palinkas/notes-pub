## File IO

#### Getting values from/to a HashMap
```java
public static ArrayList<Integer> getNumbers(String p) {
        Path path = Paths.get(p);
        ArrayList<Integer> lotteryNumbers = new ArrayList<>();

        try {
            List<String> lines = Files.readAllLines(path);
            for (String line : lines) {
                String[] lineSplit = line.split(";");

                for (int i = 11; i < 16; i++) {
                    lotteryNumbers.add(Integer.parseInt(lineSplit[i]));
                }
            }
            return lotteryNumbers;

        } catch (IOException e) {
            System.out.println(e);
        }
        return lotteryNumbers;
    }

    public static int[] commonNumbers(ArrayList<Integer> arrayList) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < arrayList.size(); i++) {
            int key = arrayList.get(i);
            if (map.containsKey(key)) {
                int freq = map.get(key);
                freq++;
                map.put(key, freq);
            } else {
                map.put(key, 1);
            }
        }
        int[] maxValues = new int[5];

        for (int i = 0; i < 5; i++) {
            int maxCount = 0, res = -1;
            for (Map.Entry<Integer, Integer> val : map.entrySet()) {
                if (maxCount < val.getValue()) {
                    res = val.getKey();
                    maxCount = val.getValue();
                }
            }
            maxValues[i] = res;
            map.remove(res);
        }
        return maxValues;
    }
```
#### Copy a file content into another file

```java
Path pathFrom = Paths.get(copyFrom);
Path pathTo = Paths.get(copyTo);
    
try {
    List<String> contentFrom = Files.readAllLines(pathFrom);
    Files.write(pathTo, contentFrom);
    return true;
    
    } catch (IOException e) {
    return false;
    }
```

## Exception handling

#### Dividing zero

```java
int num = 10;

try {
    int result = num / input;
    System.out.println(result);
    
    } catch (ArithmeticException e) {
    System.out.println("fail");
    }

```

## Unit testing

```java
package Sharpie;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SharpieTest {
  Sharpie sharpie;

  @BeforeEach
  void setUp() {
    sharpie = new Sharpie("black", 50);
  }

  @Test
  void useOnce() throws Exception {
    sharpie.use();
    assertEquals(99, sharpie.inkAmount);
  }

  @Test
  void useNone() {
    assertEquals(100, sharpie.inkAmount);
  }

  @Test
  void testUseMoreShouldThrowNoInk() {
    assertThrows(Exception.class, () -> {
      for (int i = 0; i < 200; i++) {
        sharpie.use();
      }
    });
  }
}
```


## IntelliJ shortcuts

psvm - main method

sout - Syso

fori - for loop

## Array methods
```java
//To copy the first 5 index
Arrays.copyOf(array, 5);
//to copy specific range
Arrays.copyOfRange(array, 3, 6);
//print to String
Arrays.toString(array);
//fills with a default value
Arrays.fill(array, 2);
//sort
Arrays.sort(array);
//search
Arrays.binarySearch(array, 50); //returns negative if value is not found.
```
## String methods
```java
.length()

.substring(int beginIndex) // Returns a new string that is a substring of this string.

.substring(int beginIndex, int endIndex) // Returns a new string that is a substring of this string.

.replace(char oldChar, char newChar) // Returns a new string resulting from replacing all occurrences of oldChar in this string with newChar.

.replace(CharSequence target, CharSequence replacement) // Replaces each substring of this string that matches the literal target sequence with the specified literal replacement sequence.

.charAt()

.equals() // Compares this string to the specified object.

.compare() // Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.

.toLowerCase ()

.contains()

.endsWith()

.startsWith()

.indexOf(int ch) // Returns the index within this string of the first occurrence of the specified character.

.indexOf(int ch, int fromIndex)

.indexOf(String str) // Returns the index within this string of the first occurrence of the specified substring.

.lastIndexOf()

.split(String regex) // Splits this string around matches of the given regular expression.
```
## ArrayList methods
```java
.add()

.addAll(Collection<? extends E> c)) //Appends all of the elements in the specified collection to the end of this list, in the order that they are returned by the specified collection's Iterator.

.addAll(int index, Collection<? extends E> c) //Inserts all of the elements in the specified collection into this list, starting at the specified position.

.remove() // by index, value

.set(int index, E element) //Replaces the element at the specified position in this list with the specified element.

.size()

.isEmpty()

.clear()

.contains()

.containsAll()

.toArray()

.toString()
```

## Loops

#### do while

Makes sure the code inside runs at least once.

```java
do {
(code to run)
} while (a < 10);
```
